package com.hoo.controller;

import com.hoo.entity.User;
import com.hoo.service.IUserInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 用户信息
 *
 * @author huxiangxin
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Resource
    private IUserInfoService userInfoService;

    @RequestMapping(value = "/queryUserByName")
    public User queryUserByName(String username) {
        return userInfoService.queryUserByName(username);
    }
}
