<!-- MarkdownTOC -->

1. [应用](#应用)
    1. [在 IDEA 中创建一个 web 工程](#在-idea-中创建一个-web-工程)
        1. [新建一个工程](#新建一个工程)
        1. [部署到 tomcat](#部署到-tomcat)
        1. [修改 web.xml](#修改-webxml)
        1. [新建 /WEB-INF/spring-servlet.xml](#新建-web-infspring-servletxml)
        1. [调整文件夹](#调整文件夹)

<!-- /MarkdownTOC -->

<a id="应用"></a>
# 应用
向前端暴露服务

<a id="在-idea-中创建一个-web-工程"></a>
## 在 IDEA 中创建一个 web 工程
<a id="新建一个工程"></a>
### 新建一个工程

- Create New Project 
- 选择 maven & 勾选 create from archetype，在列表中选择 `org.apache.maven.archetypes:maven-archetype-webapp`
- 填写剩下内容

<a id=""></a>
<a id="部署到-tomcat"></a>
### 部署到 tomcat

- Edit Configurations...
- `+`, Tomcat Server -> Local
- Deployment, `+`, Artifact..., XXX.war exploded

<a id="修改-webxml"></a>
### 修改 web.xml
原生创建的 web.xml 和这个很不一样，没运行起来

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://java.sun.com/xml/ns/javaee"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
        http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         version="3.0">
    <display-name>Archetype Created Web Application</display-name>
    <servlet>
        <servlet-name>spring</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>/WEB-INF/spring-servlet.xml</param-value>
        </init-param>
    </servlet>
    <servlet-mapping>
        <servlet-name>spring</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
</web-app>
```

<a id="新建-web-infspring-servletxml"></a>
### 新建 /WEB-INF/spring-servlet.xml
特别注意，如这个文件没有配置好，controller 里的路径会怎么都访问不到的

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
        http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc.xsd
        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.0.xsd">
    <!--开启 spring 的扫描注解-->
    <!--使用 @Controller, @Service, @Component ... -->
    <context:component-scan base-package="com.hoo"/>

    <!-- 开启springMVC的注解驱动，使得url可以映射到对应的controller -->
    <mvc:annotation-driven />
    
    <!--dubbo 的 bean 引入，可以用 @Autowired 注入-->
    <import resource="classpath*:user-consumer.xml" />
</beans>
```

<a id="调整文件夹"></a>
### 调整文件夹
原生创建的文件，source root 在 src 上，而且没有 main/java, main/resources 等，要自己配置

- File -> Project Structure -> Modules -> appname
- 在 Sources 标签下，可以右键创建新的文件夹，也可以通过点击 `Mark as` 里的选项来改变文件夹