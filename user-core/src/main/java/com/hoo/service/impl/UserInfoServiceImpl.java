package com.hoo.service.impl;

import com.hoo.entity.User;
import com.hoo.service.IUserInfoService;
import lombok.extern.slf4j.Slf4j;

/**
 * 用户管理
 *
 * @author huxiangxin
 */
@Slf4j
public class UserInfoServiceImpl implements IUserInfoService {

    /**
     * 通过用户名查询用户
     *
     * @param username 用户名
     * @return 用户
     */
    @Override
    public User queryUserByName(String username) {
        log.info("根据用户名查询用户 username = {}", username);
        return User.builder().age(30).mobilePhoneNumber("15801310723").username(username).build();
    }

    /**
     * 保存用户
     *
     * @param user 用户
     */
    @Override
    public void save(User user) {
        log.info("保存用户 {}", user);
    }
}
