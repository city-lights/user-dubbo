package com.hoo.service;

public interface ILoginService {

    String login(String username, String password);
}
