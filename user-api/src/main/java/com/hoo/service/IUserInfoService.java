package com.hoo.service;

import com.hoo.entity.User;

/**
 * 用户管理
 *
 * @author huxiangxin
 */
public interface IUserInfoService {

    /**
     * 通过用户名查询用户
     *
     * @param username 用户名
     * @return 用户
     */
    User queryUserByName(String username);

    /**
     * 保存用户
     *
     * @param user 用户
     */
    void save(User user);
}
