package com.hoo.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户对象
 * @author huxiangxin
 */
@Data
@Builder
public class User implements Serializable {

    private String username;

    private String password;

    private int age;

    private String mobilePhoneNumber;
}
